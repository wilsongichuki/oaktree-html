// Marketplace - http://localhost:63342/oaktree/marketplace/marketplace.html
// View/Edit product - http://localhost:63342/oaktree/products/view-edit-product.html
// Products listing - http://localhost:63342/oaktree/products/cf-products-page.html
// Add Products - http://localhost:63342/oaktree/products/add-product.html

var addProduct = "http://localhost:63342/oaktree/products/add-product.html";
var editProduct = "http://localhost:63342/oaktree/products/view-edit-product.html";
var listProducts = "http://localhost:63342/oaktree/products/cf-products-page.html";
var productStore = "http://localhost:63342/oaktree/marketplace/store.html";

var page = require("webpage").create();
page.viewportSize = { width: 1366, height: 768 };

setTimeout(function(){
    console.log('');
    console.log('Adding a product page');
    page.open(addProduct, function(){
        var renderPage = 'screenshot/' + Date.now() + '-addProduct.png';
        page.render(renderPage);
    });
}, 0);

setTimeout(function(){
    console.log('Editing a product');
    page.open(editProduct, function(){
        var renderPage = 'screenshot/' + Date.now() + '-editProduct.png';
        page.render(renderPage);
    });
}, 5000);

setTimeout(function(){
    console.log('Clicking \'toggle edit\' button');
    page.open(editProduct, function(){
        page.includeJs('http://localhost:63342/oaktree/assets/js/jquery-2.1.3.min.js', function(){
            var renderPage = 'screenshot/' + Date.now() + '-toggleEditProduct.png';
            page.render(renderPage);
        });
    });
}, 10000);

setTimeout(function(){
    console.log('Listing products');
    page.open(listProducts, function(){
        var renderPage = 'screenshot/' + Date.now() + '-listProduct.png';
        page.render(renderPage);
    });
}, 15000);

setTimeout(function(){
    console.log('Going to Marketplace');
    page.open(productStore, function(){
        page.includeJs('http://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js', function(){
            var renderPage = 'screenshot/' + Date.now() + '-store.png';
            page.render(renderPage);
        });
    });
}, 20000);

setTimeout(function () {
    console.log('Exiting...');
    page.close();
    setTimeout(function(){
        phantom.exit();
    }, 5000);
}, 25000);